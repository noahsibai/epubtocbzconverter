# EpubToCbzConveter

Python tool to automate the the conversion of .epub file to cbz. Which can be used to convert into .mobi files for Kindle PaperWhites.

## How to Use:

    - Place a list of full paths to where all of your .epub files are inside of inputdirs.txt.
        - Correct file structure should look like:
            |
            - manga-name
                |
                - folder containing epubs (this should be the directory in inputdirs.txt)
        - The final file structure will be:
            |
            - manga-name
                |
                - folder containing cbzs
                - folder containing epubs
        - Be sure to remove the line present in the file on clone.
    - Run the script and wait

## Requirements:

- Python 3.10.2+
- 7zip installed and set in Path

## Finally, you can use the Kindle Comic Converter to convert .cbz files to mobi

- You will need to download it from [Kindle Comic Converter](https://kcc.iosphe.re/)
- You will kindlegen 2.9 as a requirement for cbz to mobi conversion
