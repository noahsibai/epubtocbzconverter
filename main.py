import os
import os.path
import shutil
import subprocess

image_location = "OEBPS"


def check_if_file_exists(files, final_output_dir_files):
    for output_files in final_output_dir_files:
        for file in files:
            if file.split(".")[0] == output_files.split(".")[0]:
                files.remove(file)
    return files


def edit_extention(default_file_dir):
    os.chdir(os.path.join(default_file_dir, os.pardir) + "/cbz")
    output_files = os.listdir(os.getcwd())
    print("Changing .zip to .cbz")
    for file in output_files:
        new_file = file.split(".")[0]
        os.rename(file, new_file + ".cbz")


def move_new_files(default_file_dir, output_file_dir):
    if not os.path.isdir(
        os.path.abspath(os.path.join(default_file_dir, os.pardir)) + "/cbz"
    ):
        shutil.move(
            output_file_dir, os.path.abspath(os.path.join(default_file_dir, os.pardir))
        )
    else:
        os.chdir(output_file_dir)
        output_files = os.listdir(os.getcwd())
        final_output_dir = []
        for file in os.listdir(
            os.path.abspath(os.path.join(default_file_dir, os.pardir) + "/cbz")
        ):
            final_output_dir.append(file.split(".")[0])

        for file in output_files:
            if file.split(".")[0] not in final_output_dir:
                shutil.move(
                    file,
                    os.path.abspath(os.path.join(default_file_dir, os.pardir) + "/cbz"),
                )

    os.chdir(default_file_dir)
    shutil.rmtree(output_file_dir, ignore_errors=True)


def delete_unneeded_dirs(new_dir_names, default_file_dir):
    print("Deleting Folders Now: {new_dir_names}".format(new_dir_names=new_dir_names))
    for dir in new_dir_names:
        cur_dir = "{path}/{dir}".format(path=default_file_dir, dir=dir)
        if os.path.isdir(cur_dir):
            shutil.rmtree(cur_dir, ignore_errors=True)


def file_handling(files, default_file_dir, output_file_dir):
    new_dir_names = []
    temp_output_dir_files = os.listdir(output_file_dir)
    for file in range(len(files)):
        for output_file in temp_output_dir_files:
            if files[file].split(".")[0] == output_file.split(".")[0]:
                files.remove(files[file])
            else:
                print(
                    "{fileindex}: {file}".format(fileindex=file + 1, file=files[file])
                )

    if len(files) < 1:
        return False

    for file in files:
        filesplit = file.split(".")[0]
        new_dir_names.append(filesplit)
        subprocess.run(
            "7z x -o{filesplit} {file}".format(filesplit=filesplit, file=file),
            shell=True,
        )
        os.chdir(
            "{default_file_dir}/{filesplit}/{image_location}".format(
                default_file_dir=default_file_dir,
                filesplit=filesplit,
                image_location=image_location,
            )
        )
        potential_image_folder = os.listdir(os.getcwd())
        image_folder = ""
        for folder in potential_image_folder:
            if folder == "image" or folder == "images":
                image_folder = folder
        subprocess.run(
            "7z a -tzip ../../cbz/{filesplit} ./{image_folder}/*".format(
                filesplit=filesplit, image_folder=image_folder
            ),
            shell=True,
        )
        os.chdir(default_file_dir)
    return new_dir_names


def read_file():
    file_paths = []
    with open("inputdirs.txt") as my_file:
        file_paths = my_file.readlines()
    return file_paths


def main():
    file_paths = read_file()
    print(file_paths)

    for dirs in file_paths:
        default_file_dir = dirs.rstrip()
        output_file_dir = default_file_dir + "/cbz"
        final_output_dir_files = []

        os.chdir(default_file_dir)
        if os.path.isdir(
            os.path.abspath(os.path.join(default_file_dir, os.pardir)) + "/cbz"
        ):
            final_output_dir_files = os.listdir(
                os.path.abspath(os.path.join(default_file_dir, os.pardir)) + "/cbz"
            )
        cwd = os.getcwd()
        files = os.listdir(cwd)

        files = check_if_file_exists(files, final_output_dir_files)

        if len(files) < 1:
            continue

        if "cbz" in files:
            files.remove("cbz")

        if not os.path.isdir(output_file_dir):
            os.makedirs(output_file_dir)

        new_dir_names = file_handling(files, default_file_dir, output_file_dir)

        if not new_dir_names:
            print(
                "No new files for {default_file_dir}".format(
                    default_file_dir=default_file_dir
                )
            )
            move_new_files(default_file_dir, output_file_dir)
            continue

        delete_unneeded_dirs(new_dir_names, default_file_dir)

        move_new_files(default_file_dir, output_file_dir)

        edit_extention(default_file_dir)


if __name__ == "__main__":
    main()
